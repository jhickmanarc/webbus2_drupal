<?php
namespace Drupal\webbustests\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block for WebBus.
 *
 * @Block(
 *   id = "webbus_receive",
 *   admin_label = @Translation("WebBus receive message block"),
 *   category = @Translation("WebBus receive a message"),
 * )
 */
class WebbusReceive extends BlockBase {
    /**
    * {@inheritdoc}
    */
    public function build() {
        return [
            '#markup' => $this->construct(),
            '#attached' => [
                'library' => [
                    "webbustests/webbus-events"
                ]
            ]
        ];
    }
    
    private function construct() {
        $id = uniqid();
        return <<<EOF
<div id="{$id}" class="webbus_container" data-logic="test_listener"></div>
EOF;
    }
}
