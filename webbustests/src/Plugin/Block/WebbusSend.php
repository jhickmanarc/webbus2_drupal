<?php
namespace Drupal\webbustests\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block for WebBus.
 *
 * @Block(
 *   id = "webbus_send",
 *   admin_label = @Translation("WebBus send message block"),
 *   category = @Translation("WebBus Send a message"),
 * )
 */
class WebbusSend extends BlockBase {
    /**
    * {@inheritdoc}
    */
    public function build() {
        return [
            '#markup' => $this->construct(),
            '#attached' => [
                'library' => [
                    "webbustests/webbus-events"
                ]
            ]
        ];
    }
    
    private function construct() {
        $id = uniqid();
        return <<<EOF
<div id="{$id}" class="webbus_container" data-logic="test_sender"></div>
EOF;
    }
}