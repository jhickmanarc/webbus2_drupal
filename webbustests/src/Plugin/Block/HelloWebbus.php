<?php
namespace Drupal\webbustests\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block for WebBus.
 *
 * @Block(
 *   id = "hello_webbus",
 *   admin_label = @Translation("Hello WebBus block"),
 *   category = @Translation("Hello WebBus"),
 * )
 */
class HelloWebbus extends BlockBase {
    /**
    * {@inheritdoc}
    */
    public function build() {
        return [
            '#markup' => $this->construct(),
            '#attached' => [
                'library' => [
                    "webbustests/webbus-hello"
                ]
            ]
        ];
    }
    
    private function construct() {
        $id = uniqid();
        return <<<EOF
<div id="{$id}" class="webbus_container" data-logic="hello_world"></div>
EOF;
    }
}