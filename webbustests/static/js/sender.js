class EventSenderController extends ES6action{
    #text_input = null;

    _setup() {
        console.log("_setup called on " + this.mne);
        this.el.innerHTML = "<form><input type=\"text\" class=\"message\" /><input type=\"button\" value=\"Send!\" class=\"button_send\" /></form>";
        this.#setup();
    }

    _start() {
        console.log("_start called on " + this.nme);
    }

    #setup() {
        this.#text_input = this.el.querySelector(".message");
        this.el.querySelector(".button_send").addEventListener("click", (e) => {
            const msg = this.#text_input.value;
            this.#text_input.value = '';
            this.web_bus.fire_event('event_test', msg);
        });
    }
}