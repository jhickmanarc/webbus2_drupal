class EventListenController extends ES6action{
	_setup() {
		console.log("_setup called on " + this.nme);
	}

	_start() {
		console.log("_start called on " + this.nme);
	}

	event_test(s) {
		console.log("event_test on " + this.nme +" called and passed: " + s);
		const ne = document.createElement("div");
		ne.innerHTML = s;
		this.el.appendChild(ne);
	}
}