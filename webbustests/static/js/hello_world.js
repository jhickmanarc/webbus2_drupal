class HelloWorldController extends ES6action{
	_setup() {
		console.log("_setup called on " + this.nme);
	}

	_start() {
		console.log("_start called on " + this.nme);
		this.el.innerHTML = "Hello World named: " + this.nme;
	}
}