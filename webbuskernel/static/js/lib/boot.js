window.addEventListener('load', () => {
    const class_exists = (class_name) => {
        return new Function("return typeof " + class_name + " == 'function'")();
    }

    const components = {};
    for (let i = 0; i < webbus_components.length; i++) {
        const component_id = webbus_components[i][0];
        const component_class_name = webbus_components[i][1];
        if (class_exists(component_class_name)) {
            components[component_id] = eval(component_class_name);
        }
    }
    new WebBus(
        {
            'prototypes': components,
            'page': drupalSettings
        }
    );
});
